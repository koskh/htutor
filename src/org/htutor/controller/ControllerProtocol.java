package org.htutor.controller;

public interface ControllerProtocol {
/*
 * 
 */
	int SHOW_START =1;
	int SHOW_TOPICDESCR=2;
	int SHOW_TOPICCONTROL=3;
	int SHOW_SETTINGS=4;	
	int SHOW_ALLTOPICS=5;
}