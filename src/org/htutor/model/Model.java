package org.htutor.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import org.htutor.util.DbHelper;
import org.htutor.view.MainActivity;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class Model extends Application{
	private final String LOG_TAG="Model";
	private Context context;
	private SharedPreferences appPref;
	private DbHelper dbHelper;
	private SQLiteDatabase db;
	
	public Model(Context context){
		this.context = context;
		appPref = ((MainActivity)context).appPref;
		dbHelper= new DbHelper(context.getApplicationContext());
		db= dbHelper.getWritableDatabase();
	}
//	@Override
//	public void onCreate(){
//		super.onCreate();
//		dbHelper= new DbHelper(getApplicationContext()); 
//		db= dbHelper.getWritableDatabase();
////		setupStorage();
//	}
	public String getNameCurrentTopic(){
		String NameCurrentTopic="";// yes, we know about StringBuilder
		Cursor cr =db.query("Topic", null, "active = '1' ", null, null, null, null);
		if(cr.moveToFirst()){
		do {
			NameCurrentTopic =NameCurrentTopic +cr.getString(cr.getColumnIndex("name")) + "\n";
		} while (cr.moveToNext());
	}
	cr.close();	
	return NameCurrentTopic;
	}
	public String getDescribeCurrentTopic(){
		String DescribeCurrentTopic="";// yes, we know about StringBuilder
		Cursor cr =db.query("Topic", null, "active = '1' ", null, null, null, null);
		if(cr.moveToFirst()){
		do {
			DescribeCurrentTopic =DescribeCurrentTopic +cr.getString(cr.getColumnIndex("describe")) + " \n";
		} while (cr.moveToNext());
	}
	cr.close();	
	return DescribeCurrentTopic;
	}
//	public HashMap <Integer, String> getCurrentTopicWord(){
//		HashMap<Integer,String> currentTopicWordMap = new HashMap<Integer, String>(); 
//		Cursor cr =db.rawQuery("SELECT word.id, word.name FROM Topic,Word WHERE Topic.active=1 AND Topic.id=Word.topic", null);
//		if(cr.moveToFirst()){
//		do {
//			currentTopicWordMap.put(cr.getInt(cr.getColumnIndex("id")), cr.getString(cr.getColumnIndex("name")));
//		} while (cr.moveToNext());
//	}
//	cr.close();	
//	return currentTopicWordMap;
//	}
	public ArrayList<String[]> getCurrentTopicWord(){
		ArrayList<String[]> curWords= new ArrayList<String[]>();
		Cursor cr =db.rawQuery("SELECT word.id, word.name FROM Topic,Word WHERE Topic.active='1' AND Topic.id=Word.topic", null);
		if(cr.moveToFirst()){
		do {
//			currentTopicWordMap.put(cr.getInt(cr.getColumnIndex("id")), cr.getString(cr.getColumnIndex("name")));
			curWords.add(new String[]{ cr.getString(cr.getColumnIndex("id")), cr.getString(cr.getColumnIndex("name")) });
		} while (cr.moveToNext());
	}
	cr.close();	
	return curWords;
	}

	public String[] getWordDescription(String wordName){
		String[] worDescr =new String[9];
		Cursor cr =db.query("Word", null, "name ='"+wordName+"' ", null, null, null, null);
		if(cr.moveToFirst()){
			worDescr[0]=cr.getString(cr.getColumnIndex("id"));
			worDescr[1]=cr.getString(cr.getColumnIndex("name"));
			worDescr[2]=cr.getString(cr.getColumnIndex("transcript"));
			worDescr[3]=cr.getString(cr.getColumnIndex("translation"));
			worDescr[4]=cr.getString(cr.getColumnIndex("describe"));
			worDescr[5]=cr.getString(cr.getColumnIndex("audio"));
		}
		cr.close();
		return worDescr;
	}
	public String[] getTopicDescription(String topicName){
		String[] topicDescr =new String[5];
		Cursor cr =db.query("Topic", null, "name ='"+topicName+"' ", null, null, null, null);
		if(cr.moveToFirst()){
			topicDescr[0]=cr.getString(cr.getColumnIndex("id"));
			topicDescr[1]=cr.getString(cr.getColumnIndex("name"));
			topicDescr[2]="";//cr.getString(cr.getColumnIndex("transcript"));
			String[] descr = new String[2];
			descr =cr.getString(cr.getColumnIndex("describe")).split("#");//cr.getString(cr.getColumnIndex("translation"));
			topicDescr[3]=descr[0];
			if (descr.length==2)
					topicDescr[4]=	descr[1];
//			topicDescr[5]=cr.getString(cr.getColumnIndex("audio"));
		}
		cr.close();
		return topicDescr;
	}
	public ArrayList<String[]> getTopicControlWords(){
		String upperThreshold= appPref.getString("as_score_upper", "100");
		ArrayList<String[]> topicWords =new ArrayList<String[]>();
		Cursor cr =db.rawQuery("SELECT word.id, word.name, word.translation, word.score FROM Topic,Word WHERE Topic.active='1' AND Topic.id=Word.topic AND Word.score <" + Integer.valueOf(upperThreshold) +" ORDER BY RANDOM()", null);
		if(cr.moveToFirst()){
//			 randomTopicWords =new ArrayList<String[]>();
			do {
			topicWords.add( 
					new String[]{ 
							cr.getString(cr.getColumnIndex("id")),
							cr.getString(cr.getColumnIndex("name")),
							cr.getString(cr.getColumnIndex("translation")),
							cr.getString(cr.getColumnIndex("score"))}
					);
			} while (cr.moveToNext());
		}
		cr.close();
		// Add random word with score> upperThreshold, start puzzle from this words
		Random random = new Random();
//		String  repeat = appPref.getString("as_repeat_uppers", "1/10").split("/")[1];
//		Log.d(LOG_TAG, "repeat  "+ repeat);
		if  ( random.nextInt( Integer.valueOf( appPref.getString("as_repeat_uppers", "1/10").split("/")[1] )) ==0){
			 cr =db.rawQuery("SELECT word.id, word.name, word.translation, word.score FROM Topic,Word WHERE Topic.active='1' AND Topic.id=Word.topic AND Word.score >=" + Integer.valueOf(upperThreshold) +" ORDER BY RANDOM() LIMIT 2", null);
			 if(cr.moveToFirst()){
//				 if (randomTopicWords== null)
//					 randomTopicWords = new ArrayList<String[]>();
				do {
				topicWords.add( 
						new String[]{ 
								cr.getString(cr.getColumnIndex("id")),
								cr.getString(cr.getColumnIndex("name")),
								cr.getString(cr.getColumnIndex("translation")),
								cr.getString(cr.getColumnIndex("score"))}
						);
				} while (cr.moveToNext());
			}
			cr.close();
		}
			
//		return topicWords.isEmpty()? topicWords: null ;
		return topicWords;
	}
	public int getPuzzleType() {
		//getSharedPreferenced
		return 0; //Default ChooseLng1Btn
	}
	public void setPuzzleType(){
		//setSharedPreferenced
	}
	public ArrayList<String> getPuzleWord(int numLanguage) {
//		numLanguage // 1 for Engl, 2 for Rus
		int maxReturnedRow = 100; //we dont want  to be slow with many rows
		ArrayList<String> puzzleWords = new ArrayList<String>();
		
		Cursor cr =db.rawQuery("SELECT word.id, word.name, word.translation FROM Word  ORDER BY RANDOM() LIMIT " + maxReturnedRow, null);
		if(cr.moveToFirst()){
			do {
				String rawWord =cr.getString(numLanguage); //columnIndex= numLanguage,  need to be refactored
				rawWord.trim();
//				Log.d(LOG_TAG, "rawWord start  "+ rawWord);
//					rawWord = rawWord.replace(";",",");
//					rawWord = rawWord.replace("1)","");
//					rawWord = rawWord.replace("2)",",");
//					rawWord = rawWord.replace("3)",",");
				rawWord= rawWord.replace("\\s"," ");
				rawWord = rawWord.replaceAll(",+ *\\w\\w?\\)","#");
				rawWord = rawWord.replaceAll(";+ *\\w\\w?\\)","#");	
				rawWord= rawWord.replaceAll("^ *\\w\\w?\\) ","");
				rawWord= rawWord.replaceAll(" \\w\\w?\\)","#");
				rawWord= rawWord.replaceAll(";$","");
				rawWord= rawWord.replaceAll(",$","");
				rawWord= rawWord.replaceAll(";","#");
				rawWord= rawWord.replaceAll(",","#");
//				Log.d(LOG_TAG, "rawWord split  "+ rawWord);
				puzzleWords.addAll( Arrays.asList(rawWord.split("#")));
				
			} while (cr.moveToNext());
		}
		cr.close();
//		Log.d(LOG_TAG, "puzzleWords  ="+ puzzleWords.toString() );
		return puzzleWords;
	}

//Util
//	private void setupStorage(){
//		File datadir =  getExternalFilesDir("sound");
////		Log.d(LOG_TAG,"datadir = "+ datadir.toString());
//	}
// Utils for TopicUpdate
	public boolean isNeedUpdate(String fulltopicName) {
//		Log.d(LOG_TAG,"isNeedUpdate = "+ topicName);
		String[] topicData= fulltopicName.split("\\."); //[0] name [1]version [2]zip
		Cursor cr =db.query("Topic", null, "name = '" + topicData[0]+"'", null, null, null, null);
		if (!cr.moveToFirst()) { //we dont have this topic in BD
			cr.close();
			return true;
		}
		if ( cr.getInt(cr.getColumnIndex("version"))>= Integer.parseInt(topicData[1]) ) {
			cr.close();
			return false;
		}
		cr.close();	
		return true;
		
	}
	public void updateTopic (String topicName,String  descrFromFile, String  topicVersion){
// CREATE TABLE Topic ( id integer primary key, name text, describe text, integer version, active boolean);
		Cursor cr =db.query("Topic", null, "name = '" + topicName+"'", null, null, null, null);
//		Log.d(LOG_TAG, " //cr.getCount() ="  + cr.getCount());
		if (	cr.getCount()!=0) { //??? //UPDATE
//			Cursor ci = db.rawQuery("UPDATE Topic " +
//					"SET describe='"+descrFromFile + "', "+
//					"version='" +topicVersion +"' "+
//					"WHERE name="+ topicName
//					,null) ;
//			ci.close();
			String updateTopic ="UPDATE Topic SET describe='"+descrFromFile+"', version='"+ topicVersion+"'"+
					" WHERE name='"+topicName+"'";
			db.execSQL(updateTopic);
//			Log.d(LOG_TAG, " //UPDATE TOPIC OK " );
		}
		else {
//			Cursor cu = db.rawQuery("INSERT INTO Topic Values(null, '"+ topicName+ "',"+
//					"'"+descrFromFile+"'," +
//					"'"+topicVersion+"',1)",null) ;
//			cu.close();
//			db.in
			String insertTopic="INSERT INTO Topic Values(null, '"+ topicName+ "',"+
					"'"+descrFromFile+"','"+topicVersion+"',1)" ;
			db.execSQL(insertTopic);
//			Log.d(LOG_TAG, " //INSERT TOPIC OK " );
		}
		cr.close();
		
	}
	public void updateWord ( String topicName, String rowFromFile){
//		CREATE TABLE Word ( id integer primary key, name text, transcript text, translation text, describe text, audio text, topic integer references topic, score integer, show boolean);
//		apples#ˈæp(ə)l #яблоко#word description	
		Cursor cr =db.rawQuery("SELECT id FROM Topic WHERE name= '" + topicName+"'", null); //no error check, test only
//			if (!cr.moveToFirst())
//				return;
			cr.moveToFirst();
			int topicId= cr.getInt(cr.getColumnIndex("id"));
//			Log.d(LOG_TAG, "UPDATE WORD cr.getCount() " +topicId );
			cr.close();
		//UPDATE null score to 0
			String updateOldScore ="UPDATE Word SET score =0 WHERE score =''";
			db.execSQL(updateOldScore);
		//DELETE OLD WORD word#DELETE
			String[] row = rowFromFile.split("#"); //name#transcript#translate#description in IDEAL
			if (row.length==2 && row[1].equals("DELETE")){
				String deleteWord ="DELETE FROM Word WHERE name='"+row[0].trim()+"'";
//				Log.d(LOG_TAG,"deleteWord= " + deleteWord);
				db.execSQL(deleteWord);
				return;
			}
			
//		String[] row = rowFromFile.split("#"); //name#transcript#translate#description in IDEAL
		String name="", translate="", transcript ="",describe="";
//		Log.d(LOG_TAG, "name " +	row[0]);
			if (row.length<2 || row.length>4)
				return; //Error 
			if (row.length==2) //name#translate
				{ name=row[0].trim(); translate=row[1]; }
			if(row.length==3)
				 {name=row[0].trim();transcript=row[1]; translate=row[2]; }
			if (row.length==4)
				{name=row[0].trim();transcript=row[1]; translate=row[2]; describe=row[3]; }
//		Log.d(LOG_TAG, "name " +	row[0]+ " length= "+row.length);
		
		Cursor cw =db.rawQuery("SELECT * FROM Word WHERE name= '" + name+"' AND topic="+ topicId, null);
		if( cw.getCount()!=0){
			String updateWord=" UPDATE Word SET transcript='"+transcript+"', translation='"+translate+
					"', describe='" +describe+"' WHERE name='"+ name+"' AND topic=" + topicId;
			db.execSQL(updateWord);
//			Log.d(LOG_TAG, " //UPDATE WORD OK " );
		}
		else {
			String insertRow="INSERT INTO Word VALUES(null,'" +name+"','"+transcript+"','" + translate+
					"','"+describe+"',null,"+topicId+", 0, null)";
			db.execSQL(insertRow);
//			Log.d(LOG_TAG, " //INSERT WORD OK " );
		}
		cw.close();
		
	}
	public void close() {
		// Finish application;
//		dbHelper.close();
//		db.close();
	}
	public ArrayList< String[]> getListActiveTopic() {
		ArrayList< String[]> topicList=null;
		Cursor cr =db.query("Topic", null, "active = '1' ", null, null, null, null);
		if (cr.moveToFirst()) {
//			String [] topic= new String[cr.getCount()];
			topicList=new ArrayList<String[]>();
			
//			int i=0;
			do {
				String [] topic = new String[3];
				topic[0] = cr.getString(cr.getColumnIndex("id"));
				topic[1] = cr.getString(cr.getColumnIndex("name"));
				topic[2] = cr.getString(cr.getColumnIndex("describe"));
				topicList.add(topic);
			}  while (cr.moveToNext());
		} 
		cr.close();
		
		return topicList;
	}
	public  String[][] getListActiveWord(String topicName){
		String[][] wordList=null;
		Cursor cr =db.rawQuery("SELECT word.id, word.name, word.score FROM Topic,Word WHERE Topic.name='"+topicName+"'  AND Topic.id=Word.topic", null);
		if (cr.moveToFirst()) {
			wordList= new String[cr.getCount()][3];
			int i=0;
			do {
				wordList[i][0] = cr.getString(cr.getColumnIndex("id"));
				wordList[i][1] = cr.getString(cr.getColumnIndex("name"));
				wordList[i][2] = cr.getString(cr.getColumnIndex("score"));
				i++;
			}  while (cr.moveToNext());
		} else wordList=new String[][]{{"Слов в топике нет"}};
		cr.close();		
		return wordList;
	}
	public String[][] getAllTopics() {
		String[][] topicList =null;
		Cursor cr =db.query("Topic", null,null, null, null, null, null);
		if (cr.moveToFirst()) {
			topicList= new String[cr.getCount()][4];
			int i=0;
			do {
				topicList[i] [0]= cr.getString(cr.getColumnIndex("name"));
				topicList[i] [1]= cr.getString(cr.getColumnIndex("describe"));
				topicList[i] [2]= cr.getString(cr.getColumnIndex("version"));
				topicList[i] [3]= cr.getString(cr.getColumnIndex("active"));
//				topicList[i] [0] = "Topic1";
//				topicList[i] [1] ="1";
				Log.d(LOG_TAG, "topicList[i] [0]=  " + topicList[i] [0] + " "+ topicList[i][3]);
				i++;
			}  while (cr.moveToNext());
//	
		}
//		else {
//			topicList= new String[][]{{"Топиков нет","Описаний нет","0","0"}};
//		}
		cr.close();
		
		return topicList;
	}
	public void updateActiveTopic(String topicName, String isChecked) {
		String update ="UPDATE TOPIC SET active='"+isChecked+"' WHERE name='"+topicName+"'";
		db.execSQL(update);
	}
	
	public String getStatistic(){
//		"score>="+ appPref.getString("as_score_upper", "100")
		StringBuilder statistic = new StringBuilder();
		statistic.append("<b>Темы: </b> ");
		Cursor cr =db.query("Topic", null,"active=1", null, null, null, null);
			statistic.append(cr.getCount());
			statistic.append(" активн. / ");
			cr.close();
		cr =db.query("Topic", null,null, null, null, null, null);
			statistic.append(cr.getCount());
			statistic.append(" всего <br>");
			cr.close();
		statistic.append("<b>Слова: </b> ");
		cr =db.rawQuery("SELECT word.id, word.name, word.score FROM Topic,Word WHERE Topic.active=1 AND Topic.id=Word.topic", null);
			statistic.append(cr.getCount());
			statistic.append(" активн. / ");
			cr.close();
		cr =db.query("Word", null,null, null, null, null, null);
			statistic.append(cr.getCount());
			statistic.append(" всего <br>");
			cr.close();
		statistic.append("<b>Выучено: </b> ");
			cr =db.query("Word", null,"score>="+ appPref.getString("as_score_upper", "100"), null, null, null, null);
			statistic.append(cr.getCount());
			statistic.append(" слов <br> ");
			cr.close();
		return statistic.toString();
	}

	public boolean isFirstStart(){
		SharedPreferences shPref = getApplicationContext().getSharedPreferences("htutor", MODE_PRIVATE);
		Boolean isFirstStart =shPref.getBoolean("isFirstStart", true);
		return isFirstStart;
	}
	public void setWordScore(int wordId, int score) {
		String updateWordScore ="UPDATE WORD SET score="+score+" WHERE id="+wordId+"";
		db.execSQL(updateWordScore);
	}
	public void resetWordScore(String wordId) {
		// TODO Auto-generated method stub
		String updateWordScore ="UPDATE WORD SET score=0 WHERE id="+wordId+"";
		db.execSQL(updateWordScore);
		
	}
	public void deleteTopic(String topicName){
//		String update ="UPDATE TOPIC SET active='"+isChecked+"' WHERE name='"+topicName+"'";
		Cursor cr =db.query("Topic", null,null, null, null, null, null);
		if (cr.moveToFirst()) {
			int topicid =  cr.getInt(cr.getColumnIndex("id"));
			String deleteWords ="DELETE FROM word WHERE topic="+topicid;
				db.execSQL(deleteWords);
			String deleteTopic ="DELETE FROM Topic WHERE id="+topicid;
				db.execSQL(deleteTopic);
		}
		cr.close();
	}
}
