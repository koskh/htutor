package org.htutor.view;

import java.util.ArrayList;

import org.htutor.R;
import org.htutor.model.Model;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class TopicDescrListAdapter extends BaseExpandableListAdapter {
	private final String LOG_TAG="TopicdescrListAdapter";
	private Context context;
	private Fragment fragment;
	private Model model;
//	private Map <String, String[]> topicsMap; //[0] id, [1] name, [2] full descr
//	private Map<String, String[]> wordsMap;  //[0] id, [1] name, [2] descr
	ArrayList<String[]> topicsMap;
	ArrayList<String[][]> wordsMap;
	
	public TopicDescrListAdapter ( Context context, Fragment fragment, ArrayList<String[]>  topics,   ArrayList<String[][]>  words) {
		this.context = context;
		this.model=((MainActivity)context).model;
		this.fragment=fragment;
		this.topicsMap=topics;
		this.wordsMap= words;
	}
	@Override
	public Object getChild(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return wordsMap.get(groupPosition)[childPosition];
	}

	@Override
	public long getChildId(int arg0, int arg1) {
		return 0;
	}

	@Override
	public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, 	ViewGroup parent) {
		View v = convertView;  
		if (convertView == null) {
		        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		        v = inflater.inflate(R.layout.topicdescr_listitem, parent, false);
		    }
			( (TextView)v.findViewById(R.id.td_wordname) ).setText(wordsMap.get(groupPosition)[childPosition][1] );
			final TextView score =(TextView)v.findViewById(R.id.td_wordscore);
				score.setText(wordsMap.get(groupPosition)[childPosition][2] );
			((ImageView) v.findViewById(R.id.td_reset) ).setOnClickListener( new OnClickListener() {		
				@Override
				public void onClick(View view) {
					model.resetWordScore(wordsMap.get(groupPosition)[childPosition][0]);
						score.setText("0");
				}
			});

		return v;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		// TODO Auto-generated method stub
		return wordsMap.get(groupPosition).length;
	}

	@Override
	public Object getGroup(int groupPosition) {
		// TODO Auto-generated method stub
		return topicsMap.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		// TODO Auto-generated method stub
		return topicsMap.size();
	}

	@Override
	public long getGroupId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getGroupView( final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
		View v = convertView;  
		if (convertView == null) {
		        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		        v = inflater.inflate(R.layout.topicdescr_listheader, parent, false);
		    }
//		Set<String> topicKeys = topicsMap.keySet();
//		ArrayList<String> arrayKeys =topicKeys.toArray(array)
		( (TextView)v.findViewById(R.id.td_listheader) ).setText(topicsMap.get(groupPosition)[1]); 
		((ImageView) v.findViewById(R.id.td_help) ).setOnClickListener( new OnClickListener() {		
				@Override
				public void onClick(View v) {
					Bundle bundle = new Bundle();
					bundle.putString("topicName", topicsMap.get(groupPosition)[1] );
					DialogFragment wordDescrDialog = new WordDescrDlg();
					wordDescrDialog.setArguments(bundle);
					wordDescrDialog.show( fragment.getFragmentManager(), null);
				}
			});
//		Log.d(LOG_TAG, "name=" + topicsMap.get(groupPosition)[1]);
		return v;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isChildSelectable(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return true;
	}

}
