package org.htutor.view;

import org.htutor.R;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class TopicResultFr extends Fragment{
	 @Override
	  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View topicDescrView = inflater.inflate(R.layout.fr_topicresult,container,false);
		return topicDescrView;
	}
}
