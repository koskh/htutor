package org.htutor.view;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.htutor.R;
import org.htutor.model.Model;

import static org.htutor.controller.ControllerProtocol.*;
import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.LinearLayout.LayoutParams;

public class TopicControlFr extends Fragment{
//	public interface Listener{
//		public void topicControlExit();
//	}
// 
	private final String LOG_TAG="TopicControl Fragment";
	private Model model;
	private SharedPreferences appPref;
//	private Listener mainactivityListener;
	private ArrayList<String[]> topicWords; // word for control word into all typePUZZLES
	private String[] currentWord;
	private ArrayList<String> lng1PuzzleWords; //
	private ArrayList<String> lng2PuzzleWords; //word for buttons into ChooseRusBtn PUZZLE
	private StringBuilder missedWord;//Word with missed letterss for PUZZLE 3
	private ArrayList<String> missedLetters; //missed letters for PUZZLE 3
//Views
	private View  topicControlView;
	private TextView tc_word_control; //
//	private TextView tc_word_score; //
	private LinearLayout ll_puzzle; //
// Util
	private int maxWordPuzzleButtons=5; // quantity puzzle buttons with words
	private int columnLetterPuzleBtn=4; //quantity column puzzle buttons with letter
	private int rowLetterPuzleBtn=3; //
	private Integer currentWordScore; //
//	private int quantityMissedFields ; //
//	String letters="qwertyuiopasdfghjklzxcvbnm "; 
	Random random = new Random(); // to get random words from puzzleWords
	private int puzzleType; // ChooseLng1Btn,ChooseLng2Btn, MissingLetter
	
	 @Override
	 public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
		appPref = (	(MainActivity)getActivity()).appPref;
		model = ((MainActivity)getActivity()).model;
		topicWords= new ArrayList<String[]>();
//		puzzleType=( (MainActivity)getActivity()).pref.getInt("puzzleType", 0);
//		showPuzzle();
		Log.d(LOG_TAG,"onCreate "); 
	 }
	 
	 @Override
	  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		topicControlView = inflater.inflate(R.layout.fr_topiccontrol,container,false);
//		tc_word_score = (TextView) topicControlView.findViewById(R.id.tc_word_score);
		ImageView tc_help = (ImageView) topicControlView.findViewById(R.id.tc_help); // HRLP button
			tc_help.setOnClickListener( new OnClickListener() {
				@Override
				public void onClick(View v) {
//					nextWord();
//					showPuzzle();			
					Bundle bundle = new Bundle();
					bundle.putString("wordName", currentWord[1].trim());
//					Log.d(LOG_TAG,"currentWord[0] " +  currentWord[1].trim()); 
					DialogFragment wordDescrDialog = new WordDescrDlg();
					wordDescrDialog.setArguments(bundle);
					wordDescrDialog.show(getFragmentManager(), null);
					}
				});
		ImageView tc_directionbtn = (ImageView) topicControlView.findViewById(R.id.tc_directionbtn); //PUZZLE button
				tc_directionbtn.setOnClickListener( new OnClickListener() {
				@Override
				public void onClick(View v) {
					puzzleType= (puzzleType +1)%3;
					appPref.edit().putInt("puzzleType", puzzleType).commit();
					showPuzzle();				
					}
				});
		ImageView tc_nextbtn = (ImageView) topicControlView.findViewById(R.id.tc_nextbtn); // NEXT button
				tc_nextbtn.setOnClickListener( new OnClickListener() {
				@Override
				public void onClick(View v) {
					nextWord();
					showPuzzle();				
					}
				});
		tc_word_control = (TextView) topicControlView.findViewById(R.id.tc_word_control);
		ll_puzzle = (LinearLayout) topicControlView.findViewById(R.id.tc_puzzlebtn);
		return topicControlView;
	}
//	 @Override
//	  public void onAttach(Activity activity){
//		 super.onAttach(activity);
////		 mainactivityListener = (Listener) activity;		  
//	  }


	 @Override
		public void onStart(){
			super.onStart();
			if (topicWords.isEmpty()){  //has not rotate screen, new game
				
				puzzleType=( (MainActivity)getActivity()).appPref.getInt("puzzleType", 0); //from sharedPrefferences
				if ( ( topicWords = model.getTopicControlWords() ).isEmpty() ){ //There is no active topics
							((MainActivity)getActivity()).handler.sendEmptyMessage(SHOW_START);
							Toast.makeText(getActivity(), "Нет слов для изучения", Toast.LENGTH_SHORT).show();
//							topicWords.
							return ;
				}
//					topicWords = model.getTopicControlWords(); // get words for chooseRusBtn puzzle
					int numLanguage =1; //1 for English, 2 for Russian
					lng1PuzzleWords = model.getPuzleWord(numLanguage); //engl words for puzzle
					numLanguage =2; //1 for English, 2 for Russian
					lng2PuzzleWords = model.getPuzleWord(numLanguage); //rus words for puzzle
					nextWord();
			}
			showPuzzle();
			}
	

	private void showPuzzle() {
//	private void showPuzzle( final String[] word) {
		if (currentWord==null ){ return;} //Puzzle words is off 
		
		final String[] word=currentWord;
//		final int wordId=Integer.valueOf(word[0]);
//		currentWordScore = Integer.valueOf(word[3]);
//		Log.d(LOG_TAG, "currentScore= " + currentWordScore);
////		tc_word_score.setText(currentWordScore.toString());
		
		ll_puzzle.removeAllViews();
			
		switch(puzzleType){
		case 0: //ChooseLng1Btn	
			(	(MainActivity)getActivity()).playWord(word[1]);
			
			tc_word_control.setText( word[1].trim()); //question word
			tc_word_control.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					(	(MainActivity) getActivity()).playWord(word[1]);
				}
			});			
			int puzle1AnswerPosition= random.nextInt(maxWordPuzzleButtons-1);
			for (int i=0; i<maxWordPuzzleButtons;i++) {
//				ImageView puzzleBtn = new ImageView(getActivity());
				Button pzlBtn = new Button(getActivity());
				pzlBtn.setBackgroundResource(R.drawable.style_menubtn);
				LayoutParams lbtnparam =  new LayoutParams(	LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 1.0f);
				lbtnparam.setMargins(5, 5, 5, 5);
				pzlBtn.setLayoutParams(lbtnparam);
//				pzlBtn.setPadding(5, 50, 5, 5);
				if (i==puzle1AnswerPosition)
					pzlBtn.setText(	getAnswerText( word[2]));
				else
					pzlBtn.setText(	lng2PuzzleWords.get(	random.nextInt(	lng2PuzzleWords.size()-1)));
				pzlBtn.setOnClickListener( new OnClickListener() {					
					@Override
					public void onClick(View v) {
						String answer=((Button)v ).getText().toString();
//						int changeWordScore=0;
						if (word[2].contains(answer)) {
//							changeWordScore =  Integer.valueOf( appPref.getString("as_score_p1", "10"));
							updateWordScore(Integer.valueOf( appPref.getString("as_score_p1", "10")));
							nextWord();
							showPuzzle();
							}
						else {
//							changeWordScore =  Integer.valueOf( appPref.getString("as_score_fail", "-10"));
							updateWordScore( Integer.valueOf( appPref.getString("as_score_fail", "-10")));
						}
						
					}
				});
				ll_puzzle.addView(pzlBtn);
			}
			break;	
		case 1: //ChooseLng2Btn	
			tc_word_control.setText( getAnswerText(word[2]).trim());		
			int puzleAnswerPosition= random.nextInt(maxWordPuzzleButtons-1);
			for (int i=0; i<maxWordPuzzleButtons;i++) {
//				ImageView puzzleBtn = new ImageView(getActivity());
				Button pzlBtn = new Button(getActivity());
				pzlBtn.setBackgroundResource(R.drawable.style_menubtn);
				LayoutParams lbtnparam =  new LayoutParams(	LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 1.0f);
				lbtnparam.setMargins(5, 5, 5, 5);
				pzlBtn.setLayoutParams(lbtnparam);
				if (i==puzleAnswerPosition)
					pzlBtn.setText(getAnswerText(	word[1]));
				else
					pzlBtn.setText(	lng1PuzzleWords.get(	random.nextInt(	lng1PuzzleWords.size()-1))); //Need refactoring, We must get random english words from DB, 
				pzlBtn.setOnClickListener( new OnClickListener() {			 		
					@Override
					public void onClick(View v) {
//						int changeWordScore=0;
						String answer=((Button)v ).getText().toString();
//						Log.d(LOG_TAG,"answerLang2 " +word[2].contains(answer)); 
						if (word[1].contains(answer)){
							updateWordScore(Integer.valueOf( appPref.getString("as_score_p2", "10")));

							nextWord();
							showPuzzle();
						}
						else {
							updateWordScore( Integer.valueOf( appPref.getString("as_score_fail", "-10")));
						}
					}
				});
				ll_puzzle.addView(pzlBtn);
			}
			break;	
		case 2: //Missing letters puzzles
			(	(MainActivity)getActivity()).playWord(word[1]);
			setMissedFieldsWord( word[1]);
//				final StringBuilder puzzledWord= new StringBuilder(missedWord);
			tc_word_control.setText( missedWord);
			tc_word_control.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					(	(MainActivity) getActivity()).playWord(word[1]);	
				}
			});	
			char[] charForBtn= getCharsForBtns(); //array randomly + answer letters for buttons
			for (int r=0; r< rowLetterPuzleBtn; r++){
				LinearLayout ll_row = new LinearLayout(	getActivity());
				for (int c=0; c< columnLetterPuzleBtn; c++){
					LayoutParams lbtnparam =  new LayoutParams(	LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 1.0f);
					lbtnparam.setMargins(5, 5, 5, 5);
					Button btn = new Button(	getActivity());
						btn.setBackgroundResource(R.drawable.style_menubtn);
						btn.setLayoutParams(	lbtnparam);
						btn.setText(	charForBtn[r*(rowLetterPuzleBtn+1)+c]+"");
						btn.setOnClickListener( new OnClickListener() {
							@Override
							public void onClick(View v) {
//								String answer=((Button)v ).getText().toString();
								if (missedLetters.get(0).equals( ((Button)v ).getText().toString() )) {
									String missletter =missedLetters.remove(0);
//									tc_word_control.setText(	tc_word_control.getText().toString().replaceFirst("_",missletter));
									tc_word_control.setText (missedWord.replace( missedWord.indexOf("_"), missedWord.indexOf("_")+1, missletter));
									if ( missedLetters.size()<=0) {
										updateWordScore(Integer.valueOf( appPref.getString("as_score_p3", "10")));
										
										nextWord();
										showPuzzle();
									}
								}
								else {
									updateWordScore( Integer.valueOf( appPref.getString("as_score_fail", "-10")));
								}
							}
						});
					ll_row.addView(btn);
				}
				ll_puzzle.addView(ll_row);
			}
			break;
			}
	}
//Util methods
	private void nextWord(){
		if (topicWords.isEmpty()){
				currentWord=null;
			(	(MainActivity)getActivity()).handler.sendEmptyMessage(SHOW_START);
			return;
			}
		currentWord= topicWords.remove(topicWords.size()-1);
		currentWordScore = Integer.valueOf(currentWord[3]);
	}
	private String getAnswerText(String rawWord) { //Test only refactoring need
//		Log.d(LOG_TAG, "translateSTART ="+ translate.toString() );
		rawWord.trim();
		rawWord= rawWord.replace("\\s"," ");
		rawWord = rawWord.replaceAll(",+ *\\w\\w?\\)","#");
		rawWord = rawWord.replaceAll(";+ *\\w\\w?\\)","#");	
		rawWord= rawWord.replaceAll("^ *\\w\\w?\\) ","");
		rawWord= rawWord.replaceAll(" \\w\\w?\\)","#");
		rawWord= rawWord.replaceAll(";$","");
		rawWord= rawWord.replaceAll(",$","");
		rawWord= rawWord.replaceAll(";","#");
		rawWord= rawWord.replaceAll(",","#");
//		Log.d(LOG_TAG, "translate[] ="+ translate.toString() );
		
		String[] smallwords= rawWord.split("#");
//		Random random = new Random();
		String testword = smallwords[random.nextInt(smallwords.length)];
//		Log.d(LOG_TAG, "ttestword"+testword);
		return testword;
	}
	
	private void setMissedFieldsWord(String word){
		int quantityMissedFields = Integer.valueOf(appPref.getString("as_missed_fields", "2"));//maximum missed fields from Preferences
 			if (quantityMissedFields>word.length() )
 				quantityMissedFields= word.length();
 			
		missedLetters = new ArrayList<String>();
		missedWord = new StringBuilder(word);		
		int prevpos=0; //
		for (int i=0; i< quantityMissedFields; i++){
			int misspos = random.nextInt(	missedWord.length()-1);
			while ( misspos<prevpos)
				misspos = prevpos + random.nextInt(missedWord.length()-1 -prevpos);
			prevpos=misspos;
			if(missedWord.charAt(misspos)=='_')
				break;
			missedLetters.add(missedWord.substring(misspos, misspos+1));
			missedWord.setCharAt(misspos, '_');			
		}
//		missedWord= missword;
	}

	private char[] getCharsForBtns(){
		StringBuilder chars= new StringBuilder("qwertyuiopasdfghjklzxcvbnm ");
		char[] letterArray = new char[columnLetterPuzleBtn*rowLetterPuzleBtn];
		
		for (int i=0;i<missedLetters.size();i++){ //set missed letters
			int pos = chars.indexOf( missedLetters.get(i));
			if (pos ==-1) {
				pos=0;
				while (chars.charAt(pos)=='#')
					pos= (pos+1)%chars.length();				
			}
			letterArray[i]= chars.charAt(pos);
			chars.setCharAt(pos, '#');
		}
//		Log.d(LOG_TAG,"letterArray " +letterArray[0] + " "+ letterArray[1]);		
		for (int i=missedLetters.size(); i<columnLetterPuzleBtn*rowLetterPuzleBtn; i++)		{ //set random letters
			int pos = random.nextInt(chars.length()-1);
			while (chars.charAt(pos)=='#')
				pos=(pos+1)%chars.length();
			letterArray[i]= chars.charAt(pos);
			chars.setCharAt(pos, '#');
		}
		for (int i=0; i<letterArray.length;i++){ //mixing letters
			int mixposition=random.nextInt(letterArray.length-1);
			char temp= letterArray[i];
			letterArray[i]=letterArray[mixposition];
			letterArray[mixposition]=temp;
		}		
		return letterArray;
	}
	private void updateWordScore(int changeWordScore){
		currentWordScore = currentWordScore + Integer.valueOf(changeWordScore);
		if ( currentWordScore<0) //minimum score
			currentWordScore =0;
		if ( currentWordScore>200) //maximum score
			currentWordScore =200;
//		tc_word_score.setText(currentWordScore.toString());
//		tc_word_score.invalidate();
		model.setWordScore(Integer.valueOf(currentWord[0]), currentWordScore);
//		pause();
		
	}
	private void pause(){
		try {
			TimeUnit.SECONDS.sleep(3);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
