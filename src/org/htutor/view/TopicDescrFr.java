package org.htutor.view;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.htutor.R;
import org.htutor.model.Model;

import static org.htutor.controller.ControllerProtocol.*;
import android.app.ExpandableListActivity;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.Toast;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ListView;
import android.widget.SimpleExpandableListAdapter;
import android.widget.TextView;

public class TopicDescrFr extends Fragment {
//	 final Model model= (Model) getApplicationContext();
	final private String LOG_TAG="TopicDescrFr";
	private Model model;
	  @Override
	  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		  model= ((MainActivity)getActivity()).model;
//		  String name = model.getNameCurrentTopic();
		  View topicDescrView = inflater.inflate(R.layout.fr_topicdescr,container,false);
		  	ExpandableListView mList = (ExpandableListView) topicDescrView.findViewById(R.id.td_wordlist);
		 		   final  ArrayList <String[]> topiclist= model.getListActiveTopic();
		   if (topiclist ==null)  {//all groups are inactive
				  ((MainActivity)getActivity() ).handler.sendEmptyMessage(SHOW_START);
				  Toast.makeText(getActivity(), "Нет активных топиков", Toast.LENGTH_SHORT).show();
				  return null;
			  } 
		   
		   final  ArrayList<String[][]> wordlist =  new ArrayList<String[][]>();
		    for (String[] topic: topiclist ) {
		    	 String[][] words= model.getListActiveWord(topic[1]);
		    	 wordlist.add(words);
		    }
		    ExpandableListAdapter adapter = new TopicDescrListAdapter(getActivity(), this,  topiclist, wordlist);
		    mList.setAdapter(adapter);
		    
//		    mList.setOnGroupClickListener(new OnGroupClickListener() {
//				@Override
//				public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
//					Log.d(LOG_TAG,"group name = " +  topiclist.get(groupPosition)[1]);
//					return false;
//				}
//			});
			  mList.setOnChildClickListener( new OnChildClickListener() {			
				@Override
				public boolean onChildClick(ExpandableListView parent, View v,	int groupPosition, int childPosition, long id) {
//					Log.d(LOG_TAG,"word name = " +  wordlist.get(groupPosition)[childPosition][1]);
					Bundle bundle = new Bundle();
					bundle.putString("wordName", wordlist.get(groupPosition)[childPosition][1]  );
					DialogFragment wordDescrDialog = new WordDescrDlg();
					wordDescrDialog.setArguments(bundle);
					wordDescrDialog.show(getFragmentManager(), null);
					return false;
				}
			});
		  
		  return topicDescrView;
	}
}
