package org.htutor.view;

import static org.htutor.controller.ControllerProtocol.*;

import java.io.File;

import org.htutor.R;
import org.htutor.model.Model;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

public class TopicListAdapter extends BaseAdapter implements OnClickListener, OnLongClickListener{
	private final String LOG_TAG="ActiveTopicListAdapter";
	private Context context;
	private Fragment fragment;
	private String[][] topics;
	private LayoutInflater inflater;
	private Model model;
	
	
//	public TopicListAdapter(Context context, String[][] topics){
	public TopicListAdapter(Context context, Fragment fragment, String[][] topics){
		this.context=context;
		this.fragment = fragment;
		this.topics=topics;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.model = ((MainActivity)context).model;
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return topics.length;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return topics[position];
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
	    if (view == null) 
	    	view = inflater.inflate(R.layout.topiclist_row, parent, false);
	    
//	    view.setOnClickListener( this);
	    	
	    ((TextView) view.findViewById(R.id.al_topicname)).setText(topics[position][0]);
	    ((TextView) view.findViewById(R.id.al_descr)).setText(topics[position][1].split("#")[0]);
	    ((TextView) view.findViewById(R.id.al_version)).setText(topics[position][2]);
//	    
//	    if (topics[position][3].equals("1") )
//	    	Log.d(LOG_TAG," topics["+position+"][3]   " + topics[position][3]);
	    	if (topics[position][3].equals("1") )
	    		((CheckBox) view.findViewById(R.id.al_check)).setChecked(true);
	    	else
	    		((CheckBox) view.findViewById(R.id.al_check)).setChecked(false); 	    
//	    else 
//	    	((CheckBox) view.findViewById(R.id.al_check)).setChecked(false); 
		((CheckBox) view.findViewById(R.id.al_check)).setTag(topics[position][0]);
	    ((CheckBox) view.findViewById(R.id.al_check)).setOnClickListener(this);
	    ((CheckBox) view.findViewById(R.id.al_check)).setOnLongClickListener( this);;

		return view;
	}

	@Override
	public void onClick(View view) {
		if ( view instanceof CheckBox){
			if ( ((CheckBox) view).isChecked())
				model.updateActiveTopic(((CheckBox) view).getTag().toString(), "1" );
			else
				model.updateActiveTopic(((CheckBox) view).getTag().toString(), "0" );
//			Log.d(LOG_TAG,((CheckBox) view).getTag() + "  "+ ((CheckBox) view).isChecked());
		} 
	}
	@Override
	public boolean onLongClick(View v) {
//		final View dlgView =inflater.inflate(R.layout.dlg_setlocalfolder, null);
		final String topicName=((CheckBox) v).getTag().toString();
		final AlertDialog.Builder builder= new AlertDialog.Builder(context);
//		builder.setView(dlgView).setTitle(R.string.appsettings_localupdatefoder);
		builder.setTitle(R.string.tl_delitedlg_header);
		builder.setMessage("Удалить " + topicName + " ?");
		builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {				
			@Override
			public void onClick(DialogInterface dialog, int which) {
				model.deleteTopic(topicName);
				((MainActivity)context).handler.sendEmptyMessage(SHOW_SETTINGS);
			}
		});
		builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {				
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.cancel();
			}
		});
		AlertDialog dialog = builder.create();
		dialog.show();
	
		return true;
	}

}
