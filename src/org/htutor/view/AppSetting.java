package org.htutor.view;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.htutor.R;
import org.htutor.model.Model;
import org.htutor.util.TopicsUpdate;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.Toast;

public class AppSetting   extends Fragment  implements LoaderCallbacks<String> {
	final private String LOG_TAG="AppSettingsFr";
	private Loader <String> updateLoader;
	private Model model;
	private Bundle loaderBndl = new Bundle();
	private ArrayList <String> updateTopicList;
	private ArrayAdapter<String > updateListAdapter;
//	private Loader <String> checkTopicListUpdate;
	private SharedPreferences appPref;
	private TabHost tabHost;
		private int currentTab;
	private ImageView as_getallBtn;
//	private ImageView as_getlocaltopiclistBtn;
	private ImageView as_gettopiclistBtn;
	private ListView updateListView;
	private ProgressBar as_progress;
	private Spinner as_score_upper;
	private Spinner as_score_p1;
	private Spinner as_score_p2;
	private Spinner as_score_p3;
	private Spinner as_score_fail;
	private Spinner as_missed_fields;
	private Spinner as_repeat_uppers;
	private ImageView as_localfolder;
	private ListView showTopicList;
	private String[][] topicsListData;
	private TopicListAdapter mAdapter;
	// UTILS
	private List<String> spinnerData; //information for spinners

	public Handler handler = new Handler(){ //use for  updater showMessage
		@Override
		public void handleMessage(Message msg){
			switch(msg.what) {
			case 0:
				Toast.makeText(getActivity(),(String)msg.obj, Toast.LENGTH_SHORT).show();
				break;
			}
//			}
//			currentScreen=msg.what;
		}
	};
	@Override
	public void onCreate( Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
		appPref = (	(MainActivity)getActivity()).appPref;
		updateTopicList = new ArrayList<String>();
		model = ( (MainActivity)getActivity() ).model;
			
	}
	@Override
	  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		View appSettingsView= inflater.inflate(R.layout.fr_appsettings ,container,false);
		 tabHost= (TabHost) appSettingsView.findViewById(R.id.as_tabhost);
		 	tabHost.setup();
		 	TabHost.TabSpec tabSpec;
		 	tabSpec = tabHost.newTabSpec("tab1");
		 	tabSpec.setIndicator("Баллы");
		 	tabSpec.setContent(R.id.tab1);
		 	tabHost.addTab(tabSpec);
		 	tabSpec = tabHost.newTabSpec("tab2");
		 	tabSpec.setIndicator("Обновления");
		 	tabSpec.setContent(R.id.tab2);
		 	tabHost.addTab(tabSpec);
		 	tabSpec = tabHost.newTabSpec("tab3");
		 	tabSpec.setIndicator("Топики");
		 	tabSpec.setContent(R.id.tab3);
		 	tabHost.addTab(tabSpec);
		 	tabHost.setCurrentTab(currentTab);
		 	
		 	tabHost.setOnTabChangedListener( new OnTabChangeListener() {			
				@Override
				public void onTabChanged(String tabId) {
					//Log.d(LOG_TAG,"onTabChanged topic"+ tabId );
					if ( tabId.equals("topictab3"))
						fillControlTopicList();
				}
			});
		 
//		TAB 1
		 	as_score_upper = (Spinner) appSettingsView.findViewById(R.id.as_score_upper);
		 	as_score_p1 = (Spinner) appSettingsView.findViewById(R.id.as_score_p1);
		 	as_score_p2 = (Spinner) appSettingsView.findViewById(R.id.as_score_p2);
	 		as_score_p3 = (Spinner) appSettingsView.findViewById(R.id.as_score_p3);
	 		as_score_fail = (Spinner) appSettingsView.findViewById(R.id.as_score_fail);
	 		as_missed_fields = (Spinner) appSettingsView.findViewById(R.id.as_missed_fields);
	 		as_repeat_uppers = (Spinner) appSettingsView.findViewById(R.id.as_repeat_uppers);
	 		setupTab1();
		 	
//	 	TAB 2
	 	updateListView=(ListView) appSettingsView.findViewById(R.id.as_topicslist);
	 	as_localfolder = (ImageView) appSettingsView.findViewById(R.id.as_localfolder);
	 	as_progress = (ProgressBar)appSettingsView.findViewById(R.id.as_progress);	
	 	as_gettopiclistBtn = (ImageView) appSettingsView.findViewById(R.id.as_gettopiclist);
	 	as_getallBtn = (ImageView) appSettingsView.findViewById(R.id.as_getall);
	 	
	 	setupTab2();
		
//		TAB 3
	 	showTopicList = (ListView) appSettingsView.findViewById(R.id.as_showtopiclist);
		setupTab3(); 	
	 	
		 	
		 return appSettingsView;		 
	 }
	
	@Override
	public void onStart(){
		  super.onStart();
//			getAllUpdateLoader =getLoaderManager().initLoader(1, null, this);
		  if ( getLoaderManager().getLoader(0) != null) {
			  updateLoader =getLoaderManager().initLoader(0, null, this);
			  as_progress.setVisibility(ProgressBar.VISIBLE); // update is currently running
		  }
		  else if ( getLoaderManager().getLoader(1) != null) {
			  updateLoader =getLoaderManager().initLoader(1, null, this);
			  as_progress.setVisibility(ProgressBar.VISIBLE); // update is currently running
			  }
		  else if ( getLoaderManager().getLoader(2) != null) {
			  updateLoader =getLoaderManager().initLoader(2, null, this);
			  as_progress.setVisibility(ProgressBar.VISIBLE); // update is currently running
			  }
		  fillUpdateTopicList(); 
	}

	@Override
	public void onPause(){
		super.onPause();
		//Save Preferences
		Editor edPref= appPref.edit();
			edPref.putString("as_score_upper", as_score_upper.getSelectedItem().toString());
			edPref.putString("as_score_p1", as_score_p1.getSelectedItem().toString());
			edPref.putString("as_score_p2", as_score_p2.getSelectedItem().toString());
			edPref.putString("as_score_p3", as_score_p3.getSelectedItem().toString());
			edPref.putString("as_score_fail",as_score_fail.getSelectedItem().toString());
			edPref.putString("as_missed_fields",as_missed_fields.getSelectedItem().toString());
			edPref.putString("as_repeat_uppers",as_repeat_uppers.getSelectedItem().toString());
			edPref.commit();
		currentTab = tabHost.getCurrentTab();
	}
// Listeners
		@Override
		public Loader<String> onCreateLoader(int id, Bundle bndl) {
				loaderBndl.putString("updateLocalFolder",appPref.getString("updateLocalFolder", "/mnt/") ); //Folder for local updaters
				return new TopicsUpdate(getActivity(), handler ,bndl);
		}
		@Override
		public void onLoadFinished(Loader<String> loader, String arg1) {
			as_gettopiclistBtn.setClickable(true);
			as_getallBtn.setClickable(true);
			as_progress.setVisibility(ProgressBar.INVISIBLE);
			
			if ( loader.getId()==0 ) {
					//Toast.makeText(getActivity(),  ((CheckTopicListUpdate)updateLoader).message, Toast.LENGTH_SHORT).show();
				this.updateTopicList= ((TopicsUpdate)updateLoader).updateTopicList;	
				fillUpdateTopicList();
				getLoaderManager().destroyLoader(0);
//					((MainActivity)getActivity() ).handler.sendEmptyMessage(SHOW_ALLTOPICS);
//				}
			}
			if ( loader.getId()==1 ) {
//				Toast.makeText(getActivity(),  ((TopicsUpdate)updateLoader).message +"\n успешно обновлен", Toast.LENGTH_SHORT).show();
				updateListAdapter.remove( ((TopicsUpdate)updateLoader).message);
				updateListAdapter.notifyDataSetChanged();
				getLoaderManager().destroyLoader(1);
				updateListView.setEnabled(true);
				fillControlTopicList();
//				((MainActivity)getActivity() ).handler.sendEmptyMessage(SHOW_ALLTOPICS);
//			}
		}
			if ( loader.getId()==2 ) {
//				as_gettopiclistBtn.setClickable(true);
//				as_getallBtn.setClickable(true);
//				as_progress.setVisibility(0);
//					Toast.makeText(getActivity(),  ((TopicsUpdate)updateLoader).message, Toast.LENGTH_SHORT).show();
					updateListAdapter.clear();
					updateListAdapter.notifyDataSetChanged();
					getLoaderManager().destroyLoader(2);
					updateListView.setEnabled(true);
					fillControlTopicList();
//					tabHost.setCurrentTab(3);
//					((MainActivity)getActivity() ).handler.sendEmptyMessage(SHOW_ALLTOPICS);
//				}
			}

		}
		@Override
		public void onLoaderReset(Loader<String> arg0) {
			// TODO Auto-generated method stub
//			sa_updatebtn.setEnabled(true);
//			Log.d(LOG_TAG, "onLoaderReset ");
		}

//UTILS 
private void setupTab1(){
//		as_score_upper = (Spinner) appSettingsView.findViewById(R.id.as_score_upper);
		spinnerData =Arrays.asList(getActivity().getResources().getStringArray(R.array.as_score_upper));
		ArrayAdapter<String> upperadapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, spinnerData);
			as_score_upper.setAdapter(upperadapter);
			as_score_upper.setPromptId(R.string.appsettings_upperThreshold);
			as_score_upper.setSelection( spinnerData.indexOf( appPref.getString("as_score_upper", "100")));
//		as_score_p1 = (Spinner) appSettingsView.findViewById(R.id.as_score_p1);
			spinnerData =Arrays.asList(getActivity().getResources().getStringArray(R.array.as_score_puzzle));  
	 		ArrayAdapter<String> scoreadapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, spinnerData);
	 		as_score_p1.setAdapter(scoreadapter);
	 		as_score_p1.setPromptId(R.string.appsettings_puzzle1);
	 		as_score_p1.setSelection( spinnerData.indexOf( appPref.getString("as_score_p1", "10")));
//	 	as_score_p2 = (Spinner) appSettingsView.findViewById(R.id.as_score_p2);
 			as_score_p2.setAdapter(scoreadapter);
 			as_score_p2.setPromptId(R.string.appsettings_puzzle2);
 			as_score_p2.setSelection( spinnerData.indexOf( appPref.getString("as_score_p2", "10")));
// 		as_score_p3 = (Spinner) appSettingsView.findViewById(R.id.as_score_p3);
 			as_score_p3.setAdapter(scoreadapter);
 			as_score_p3.setPromptId(R.string.appsettings_puzzle3);
 			as_score_p3.setSelection( spinnerData.indexOf( appPref.getString("as_score_p3", "10")));
// 		as_score_fail = (Spinner) appSettingsView.findViewById(R.id.as_score_fail);
 			spinnerData = Arrays.asList(getActivity().getResources().getStringArray(R.array.as_score_fail)); 
 			ArrayAdapter<String> scorefailadapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, spinnerData);
 			as_score_fail.setAdapter(scorefailadapter);
 			as_score_fail.setPromptId(R.string.appsettings_puzzlefail);
 			as_score_fail.setSelection( spinnerData.indexOf( appPref.getString("as_score_fail", "-10")));
// 		as_missed_fields = (Spinner) appSettingsView.findViewById(R.id.as_missed_fields);
 			spinnerData = Arrays.asList(getActivity().getResources().getStringArray(R.array.as_missed_fields)); 
 			ArrayAdapter<String> missedadapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, spinnerData);
 			as_missed_fields.setAdapter(missedadapter);
 			as_missed_fields.setPromptId(R.string.appsettings_missed_fields);
 			as_missed_fields.setSelection( spinnerData.indexOf( appPref.getString("as_missed_fields", "2")));
// 		as_repeat_uppers = (Spinner) appSettingsView.findViewById(R.id.as_repeat_uppers);
 			spinnerData = Arrays.asList(getActivity().getResources().getStringArray(R.array.as_repeat_uppers)); 
 			ArrayAdapter<String> repeatadapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, spinnerData);
 			as_repeat_uppers.setAdapter(repeatadapter);
 			as_repeat_uppers.setPromptId(R.string.appsettings_repeat_uppers);
 			as_repeat_uppers.setSelection( spinnerData.indexOf( appPref.getString("as_repeat_uppers", "1/10")));
 		
}
private void setupTab2(){
		as_gettopiclistBtn.setOnClickListener( new OnClickListener() {
	@Override
	public void onClick(View v) {
//		Toast.makeText(getActivity(),  "Запрос обновлений топиков", Toast.LENGTH_SHORT).show();
//		loaderBndl.putString("command", "getUpdateTopicList");
		updateLoader =getLoaderManager().restartLoader(0, loaderBndl, AppSetting.this);
		updateLoader.forceLoad();
		as_gettopiclistBtn.setClickable(false);
		as_getallBtn.setClickable(false);
		as_progress.setVisibility( ProgressBar.VISIBLE);
//		((MainActivity)getActivity() ).handler.sendEmptyMessage(SHOW_SETTINGS);
		}
 	});	
//	as_getlocaltopiclistBtn.setOnClickListener( new OnClickListener() {
//	@Override
//	public void onClick(View v) {
//		Toast.makeText(getActivity(),  "Запрос локальных обновлений топиков", Toast.LENGTH_SHORT).show();
////		loaderBndl.putString("command", "getUpdateTopicList");
//		updateLoader =getLoaderManager().restartLoader(3, loaderBndl, AppSetting.this);
//		updateLoader.forceLoad();
//		as_gettopiclistBtn.setClickable(false);
//		as_getlocaltopiclistBtn.setClickable(false);
//		as_getallBtn.setClickable(false);
//		as_progress.setVisibility( ProgressBar.VISIBLE);
////		((MainActivity)getActivity() ).handler.sendEmptyMessage(SHOW_SETTINGS);
//		}
// 	});	
 	as_getallBtn.setOnClickListener( new OnClickListener() {
	@Override
	public void onClick(View v) {
//		Toast.makeText(getActivity(),  "Начинаем обновление всех топиков.", Toast.LENGTH_SHORT).show();
		loaderBndl.putString("command", "updateAllTopics");
		updateLoader =getLoaderManager().restartLoader(2, loaderBndl, AppSetting.this);
		updateLoader.forceLoad();
		as_gettopiclistBtn.setClickable(false);
		as_getallBtn.setClickable(false);
		as_progress.setVisibility(ProgressBar.VISIBLE);
		updateListView.setEnabled(false);
//		((MainActivity)getActivity() ).handler.sendEmptyMessage(SHOW_SETTINGS);
		}
 	});
 	
 	as_localfolder.setOnClickListener( new OnClickListener() {
		@Override
		public void onClick(View v) {
			final View dlgView = LayoutInflater.from(getActivity()).inflate(R.layout.dlg_setlocalfolder, null);
			final EditText input = (EditText) dlgView.findViewById(R.id.as_dlg_localfolderinput);
				String localFolder= appPref.getString("updateLocalFolder", "/");
				input.setText(localFolder);
			final AlertDialog.Builder builder= new AlertDialog.Builder(getActivity());
			builder.setView(dlgView).setTitle(R.string.appsettings_localupdatefoder);
			builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					String newPath= input.getText().toString().replaceAll("/+$","");
						if ( ( new File (newPath) ).isDirectory() ) {
							Editor edPref= appPref.edit();
							edPref.putString("updateLocalFolder", newPath);
							edPref.commit();
						}
				}
			});
			builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					dialog.cancel();
				}
			});
			AlertDialog dialog = builder.create();
			dialog.show();
		}	
	});
// 	updateListView.setLa
}
private void setupTab3(){
	 topicsListData = model.getAllTopics();
	  if (topicsListData !=null)  {//all groups are not inactive
//		 // Toast.makeText(getActivity(), "Нет топиков, но можно скачать\n (Настройки-> Обновления)", Toast.LENGTH_SHORT).show();
	   mAdapter = new TopicListAdapter(getActivity(),this, topicsListData);
	  showTopicList.setAdapter(mAdapter);
	  }
	
}
private void fillUpdateTopicList(){
//	Log.d(LOG_TAG,"updateTopicList"+ updateTopicList);
	updateListAdapter = new ArrayAdapter<String>(getActivity(), R.layout.updtopiclist_row, R.id.as_updtopicname, updateTopicList);
	updateListView.setAdapter(updateListAdapter);
	updateListView.setOnItemClickListener( new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,	int position, long id) {
//			Log.d(LOG_TAG,"get topic"+ ((CheckTopicListUpdate)updateLoader).downloadTopic.get(position));
				updateListView.setEnabled(false);
				as_progress.setVisibility(ProgressBar.VISIBLE);
				loaderBndl.putString("command", updateTopicList.get(position) );
				updateLoader =getLoaderManager().restartLoader(1, loaderBndl, AppSetting.this);
					updateLoader.forceLoad();
			}
	 	});
}
private void fillControlTopicList() {
	 topicsListData = model.getAllTopics();
	  if (topicsListData !=null)  
		  mAdapter = new TopicListAdapter(getActivity(),this, topicsListData);
	  showTopicList.setAdapter(mAdapter);
}

}