package org.htutor.view;

import org.htutor.R;
import org.htutor.model.Model;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;

public class WordDescrDlg extends DialogFragment implements OnClickListener {
	final String LOG_TAG = "WordDescription Dialog";
//	private int wordId;

	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState){
//		int wordId= getArguments().getInt("wordId");
		Model model= ((MainActivity) getActivity()).model;
		String[] Description;
		if (getArguments().containsKey("wordName")) //{ 
//			String wordName = getArguments().getString("wordName");
			Description = model.getWordDescription(getArguments().getString("wordName") ); // {id, name, transcript,translate,describe,audio}
		else
			Description = model.getTopicDescription(getArguments().getString("topicName") );
//		getDialog().setTitle(wordDescription[1]);
		getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE); 
		View  worddescrView = inflater.inflate(R.layout.dlg_wordview, null);
		TextView name = (TextView) worddescrView.findViewById(R.id.wordview_name);
			name.setText(Description[1]);
		TextView transcript = (TextView) worddescrView.findViewById(R.id.wordview_transcr);
			String fontPath="fonts/segoeui.ttf";
			Typeface typeface=Typeface.createFromAsset(getActivity().getAssets(), fontPath);
			transcript.setTypeface(typeface);
			transcript.setText(Description[2]);
		TextView translate = (TextView) worddescrView.findViewById(R.id.wordview_translt);
			translate.setText(Description[3]);
		TextView description = (TextView) worddescrView.findViewById(R.id.wordview_descrpt);
			description.setText(Description[4]);
		worddescrView.findViewById(R.id.wordview_btnclose).setOnClickListener(this);
		
		return worddescrView;
	}
	@Override
	public void onStart(){
		super.onStart();
		getDialog().getWindow().setLayout(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
//		getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE); 
		getDialog().getWindow().setBackgroundDrawableResource(R.drawable.img_dlg_back);
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		dismiss();
	}
	

}
