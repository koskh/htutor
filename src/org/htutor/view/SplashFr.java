package org.htutor.view;

import java.io.File;
import java.io.InputStream;

import org.htutor.R;

import static org.htutor.controller.ControllerProtocol.SHOW_START;
import android.app.Activity;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class SplashFr extends Fragment {
	private final String LOG_TAG="SPLASH_FR";
//	public interface Listener{
//		public void setScreen(String tag);
//	}
//	private Listener listener;
	private Loader < String> loader;
	private Handler handler ; //dialog's handler for message from ActivSyncLoader
	 
	 @Override
	 public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
		
	boolean isFirstStart=(	(MainActivity)getActivity()).appPref.getBoolean("isFirstStart", true);
		  if (isFirstStart){
			setupStorage(); 
//			unpackStartTopic();
			( (MainActivity)getActivity()).appPref.edit().putBoolean("isFirstStart", false).commit();
		  }
	// setting up is done. show main screen	  
	( (MainActivity)getActivity()).handler.sendEmptyMessage(SHOW_START);
  }
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	View startAppView = inflater.inflate(R.layout.fr_splash,container,false);
	return startAppView;
}
//  @Override
//  public void onStart(){
//	  super.onStart();
//  }
  
//UTIL
private void setupStorage(){	
	//Create FOLDERS for AUDIO
	File tempDir = getActivity().getDir("temp", 0);
//	File soundDir =getActivity().getExternalFilesDir("sound");
	File soundDir =getActivity().getDir("sound", 0);
	StringBuilder letters=new StringBuilder("qwertyuiopasdfghjklzxcvbnm");
	for (int i=0; i<letters.length();i++){
		String name= letters.substring(i, i+1);
		File folder = new File (soundDir+"/"+name);
//		Log.d (LOG_TAG,"folder =" + folder.toString());
		folder.mkdir();
		}	  
	// default folder for local update
//	File localUpdFolder = new File (Environment.getExternalStorageDirectory().toString() +"/HTutorLocalUpdate" );
//		localUpdFolder.mkdir();
		if (Environment.getExternalStorageDirectory().isDirectory())
			( (MainActivity)getActivity()).appPref.edit().putString("updateLocalFolder", Environment.getExternalStorageDirectory().toString()).commit();
		else 
			( (MainActivity)getActivity()).appPref.edit().putString("updateLocalFolder","/").commit();
		
	}
}
