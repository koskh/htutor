package org.htutor.view;

import static org.htutor.controller.ControllerProtocol.SHOW_SETTINGS;
import static org.htutor.controller.ControllerProtocol.SHOW_TOPICCONTROL;
import static org.htutor.controller.ControllerProtocol.SHOW_TOPICDESCR;

import org.htutor.R;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
//import org.htutor.util.DownloadTopic;

public class StartAppFr extends Fragment  {
	private final String LOG_TAG="StartAppFr";

//	private Handler handler;
	private ImageView sa_settingsBtn;
	private ImageView sa_controlBtn;
	private ImageView sa_wordsBtn;
	private TextView  sa_statistic;

	  @Override
		 public void onCreate(Bundle savedInstanceState){
			super.onCreate(savedInstanceState);
			setRetainInstance(true);
	  }
	  @Override
	  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View startAppView = inflater.inflate(R.layout.fr_startapp,container,false);
		sa_settingsBtn = (ImageView) startAppView.findViewById(R.id.sa_setting);
		 	sa_settingsBtn.setOnClickListener( new OnClickListener() {
			@Override
			public void onClick(View v) {			
				((MainActivity)getActivity() ).handler.sendEmptyMessage(SHOW_SETTINGS);
			}
		});
		sa_controlBtn = (ImageView) startAppView.findViewById(R.id.sa_control);
		 	sa_controlBtn.setOnClickListener( new OnClickListener() {
				@Override
				public void onClick(View v) {
					((MainActivity)getActivity() ).handler.sendEmptyMessage(SHOW_TOPICCONTROL);
				}
			});
		sa_wordsBtn = (ImageView) startAppView.findViewById(R.id.sa_words);
			sa_wordsBtn.setOnClickListener( new OnClickListener() {
				@Override
				public void onClick(View v) {
					((MainActivity)getActivity() ).handler.sendEmptyMessage(SHOW_TOPICDESCR);
				}
			});	
			
			sa_statistic= (TextView) startAppView.findViewById(R.id.sa_statistic);
			sa_statistic.setText( Html.fromHtml(	((MainActivity)getActivity() ).model.getStatistic() ) );
			
//			sa_showallopics.setOnClickListener( new OnClickListener() {
//				@Override
//				public void onClick(View v) {
//					((MainActivity)getActivity() ).handler.sendEmptyMessage(SHOW_ALLTOPICS);
//				}
//			});	
	return startAppView;
}
	
	  


}
