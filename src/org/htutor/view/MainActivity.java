package org.htutor.view;

import java.util.ArrayList;
import java.util.Currency;

import org.htutor.R;
import org.htutor.model.Model;
import org.htutor.util.Sound;

import static org.htutor.controller.ControllerProtocol.*;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.Menu;

public class MainActivity extends FragmentActivity {
	final private String LOG_TAG ="MainActivity";
	public SharedPreferences appPref;
	public Model model;
	private Sound sound;

	private String currentScreen;
	public Handler handler = new Handler(){
		@Override
		public void handleMessage(Message msg){
			switch(msg.what) {
			case SHOW_START:	setScreen("StartAppFr"); break;
			case SHOW_TOPICDESCR:	setScreen("TopicDescrFr"); break;
			case SHOW_TOPICCONTROL:	setScreen("TopicControlFr"); break;
			case SHOW_SETTINGS:	setScreen("AppSetting"); break;
			case SHOW_ALLTOPICS:	setScreen("TopicListFr"); break;
			}
//			currentScreen=msg.what;
		}
	};
//	private  DialogFragment firstStartDlg  = new FirstStartDlg();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
//		Log.d(LOG_TAG, "onCreate");
		super.onCreate(savedInstanceState);
		if (savedInstanceState!=null) //normal start application, NOt rotateScreen
			currentScreen=savedInstanceState.getString("currentScreen");
		appPref = getApplicationContext().getSharedPreferences("htutor", MODE_PRIVATE);
		model = new Model(this);
		sound= new Sound(this);
		
		setContentView(R.layout.l_mainactivity);		
		if (currentScreen==null) 
			setScreen("SplashFr");
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}


// Utils
	public void setScreen(String tag){
		FragmentManager fm = getSupportFragmentManager();
			FragmentTransaction fmtransact = fm.beginTransaction();
			if ( fm.findFragmentById(R.id.bodyfrgm_fl) !=null)
				fmtransact.detach(fm.findFragmentById(R.id.bodyfrgm_fl));
		Fragment fragment = fm.findFragmentByTag(tag);
		
		try{
			if ( fragment==null) {			
				fmtransact.add(R.id.bodyfrgm_fl, (Fragment) Class.forName("org.htutor.view."+tag).newInstance(),tag);				
			} else {
//				fmtransact.detach(fm.findFragmentById(R.id.bodyfrgm_fl));
				fmtransact.attach(fragment);
				}
			} catch  (Exception e) {  Log.d(LOG_TAG,e.toString());}

//			fmtransact.addToBackStack(tag);
			fmtransact.commit();
//			
////		NavigateMenu menuBtnFr = (NavigateMenu)getSupportFragmentManager().findFragmentById(R.id.navigate_menu_fr);
////			menuBtnFr.reloadMenu(tag);
		currentScreen = tag;
//		menubtnPress("loadFragment", tag);
	}

// Util
	public void playWord(String word){
		sound.playWord(word);
//		File soundfile= sound.getSoundFile(word);	
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putString("currentScreen", currentScreen);
		super.onSaveInstanceState(outState);
	}

	@Override
	public void onBackPressed() {
//		 super.onBackPressed();
		if (!currentScreen.equals("StartAppFr"))
			setScreen("StartAppFr");
		else {
//			model.close();
			finish();
		}
	}
	
}
