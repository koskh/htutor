package org.htutor.view;

import static org.htutor.controller.ControllerProtocol.SHOW_START;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.htutor.R;
import org.htutor.model.Model;

import android.app.ExpandableListActivity;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.Toast;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ListView;
import android.widget.SimpleExpandableListAdapter;
import android.widget.TextView;

public class TopicListFr extends Fragment {
//	 final Model model= (Model) getApplicationContext();
	final private String LOG_TAG="TopicListFr";
	private Model model;
	  @Override
	  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		  model= ((MainActivity)getActivity()).model;
//		  String name = model.getNameCurrentTopic();
		  View topicDescrView = inflater.inflate(R.layout.fr_topiclist,container,false);
//		  TextView currentName= (TextView) topicDescrView.findViewById(R.id.sa_updateinfo);
//		  currentName.setText(model.getNameCurrentTopic() );
		  
//		  ExpandableListView mList = (ExpandableListView) topicDescrView.findViewById(R.id.td_wordlist);
		 
//		  TextView currentDescr= (TextView) topicDescrView.findViewById(R.id.td_descr);
//		  currentDescr.setText(model.getDescribeCurrentTopic() );
//		  
//		  String[][] topics =new String[][]{ {"Topic1","true"},{"Topic2","true"}, {"Topic3","false"}};
		  String[][] topics = model.getAllTopics();

		  if (topics ==null)  {//all groups are inactive
			  ((MainActivity)getActivity() ).handler.sendEmptyMessage(SHOW_START);
			  Toast.makeText(getActivity(), "Нет топиков, но можно скачать\n (Настройки-> Обновления)", Toast.LENGTH_SHORT).show();
			  return null;
		  } 
		  final ListView al_activetopiclist = (ListView) topicDescrView.findViewById(R.id.al_topiclist);
		  TopicListAdapter mAdapter = new TopicListAdapter(getActivity(),this, topics);
		  al_activetopiclist.setAdapter(mAdapter);
		 
		  
		  
		  return topicDescrView;
	}
}
