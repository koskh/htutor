package org.htutor.view;

import java.util.ArrayList;
import java.util.HashMap;

import org.htutor.R;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

public class NavigateMenu extends Fragment {
//	public interface Listener{
//		public void menubtnPress(String command,String tag);
//	}
	  final String LOG_TAG = "NavigateMenu";
//	  final int numButtons=4;//How many buttons in menu we show
	  final HashMap<String, ArrayList<NavigateMenu.menubtnDescr> > menuBtnsDescr = setButtons(); //We will show different buttons in activities
	  private FragmentTransaction frgmtransact;
//	  private  Listener mainactivityListener;
	  private View menuBtnView;
	  
	  @Override
	  public void onAttach(Activity activity){
		 super.onAttach(activity);
//		 mainactivityListener = (Listener) activity;
		  
	  }
	  @Override
		 public void onCreate(Bundle savedInstanceState){
			super.onCreate(savedInstanceState);
			setRetainInstance(true);
//			Log.d(LOG_TAG, "onCreate " + this.toString());
	  }
	  @Override
	  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		menuBtnView = inflater.inflate(R.layout.fr_navigate_menu,container,false);
		constructMenu("StartAppFr");
		
		return menuBtnView;
	}
	  
	 //Utils
	  private class menubtnDescr{ //MenuButton description
		  int icon; //R.drawable.img
		  int name; //R.string
//		  String command;
		  int tag;
		  public menubtnDescr(int icon, int name, int tag ) {
			this.icon=icon;
			this.name=name;
			this.tag=tag;
//			this.command=command;
		}
	  }
	 
	  private HashMap<String, ArrayList<menubtnDescr>> setButtons(){
//		  HashMap<String, ArrayList<menubtnDescr> > menuDescr= new HashMap<String, ArrayList<menubtnDescr>>();
//		  //Test only
//		  ArrayList<NavigateMenu.menubtnDescr> menuStartActivity = new ArrayList<NavigateMenu.menubtnDescr>();
//		  menuStartActivity.add(new menubtnDescr(R.drawable.img_topicdescr, R.string.topic_descr,2));
//		  menuStartActivity.add(new menubtnDescr(R.drawable.img_topiccontorl, R.string.topic_control,3));
////		  menuStartActivity.add(new menubtnDescr(R.drawable.img_motivation, R.string.topic_result,"loadFragment","TopicResultFr"));
//		  menuStartActivity.add(new menubtnDescr(R.drawable.img_appsetting, R.string.app_settings,4));
//		  
//		  ArrayList<NavigateMenu.menubtnDescr> menuTopicDescr = new ArrayList<NavigateMenu.menubtnDescr>();
//		  menuTopicDescr.add(new menubtnDescr(R.drawable.img_topiccontorl, R.string.topic_control,3));
//		  menuTopicDescr.add(new menubtnDescr(R.drawable.img_motivation, R.string.app_settings,4));
//		  menuTopicDescr.add(new menubtnDescr(R.drawable.img_back, R.string.app_start,1));
////		  menuTopicDescr.add(new menubtnDescr(R.drawable.img_appsetting, R.string.app_settings,"loadFragment","AppSetting"));
//		  
//		  ArrayList<NavigateMenu.menubtnDescr> menuTopicControl = new ArrayList<NavigateMenu.menubtnDescr>();
//		  menuTopicControl.add(new menubtnDescr(R.drawable.img_topiccontorl, R.string.topic_descr,2));
//		  menuTopicControl.add(new menubtnDescr(R.drawable.img_motivation, R.string.topic_control,3));
////		  menuTopicControl.add(new menubtnDescr(R.drawable.img_appsetting, R.string.app_settings,"","AppSetting"));
////		  menuTopicControl.add(new menubtnDescr(R.drawable.img_appsetting, R.string.app_settings,"","AppSetting"));
//		  menuTopicControl.add(new menubtnDescr(R.drawable.img_back, R.string.app_start,1 ));
//		  
////		  ArrayList<NavigateMenu.menubtnDescr> menuTopicResult = new ArrayList<NavigateMenu.menubtnDescr>();
////		  menuTopicResult.add(new menubtnDescr(R.drawable.img_topicdescr, R.string.topic_descr,"loadFragment","TopicDescrFr"));
////		  menuTopicResult.add(new menubtnDescr(R.drawable.img_back, R.string.app_start,"loadFragment","StartAppFr"));
////		  menuTopicResult.add(new menubtnDescr(R.drawable.img_topiccontorl, R.string.topic_control,"loadFragment","TopicControlFr"));
////		  menuTopicResult.add(new menubtnDescr(R.drawable.img_appsetting, R.string.app_settings,"loadFragment","AppSetting"));
//		  
		  
//		  menuDescr.put("StartAppFr", menuStartActivity);
//		  menuDescr.put("TopicDescrFr", menuTopicDescr);
//		  menuDescr.put("TopicControlFr", menuTopicControl);
////		  menuDescr.put("AppSetting", menuTopicResult);
//		  return menuDescr;
		  return null;
	  }
	 
	  private void constructMenu( String currentActivityFragment){
		  ViewGroup menuBtnLL = (ViewGroup) menuBtnView.findViewById(R.id.menuBtn_ll);
		  menuBtnLL.removeAllViews();
//		  String activeFragment="StartAppFr";

		  if (menuBtnsDescr.containsKey(currentActivityFragment)) {
//			  Log.d(LOG_TAG,"menuBtnsDescr.containsKey OK");
			ArrayList<NavigateMenu.menubtnDescr> menudescr = menuBtnsDescr.get(currentActivityFragment);
			for (NavigateMenu.menubtnDescr btn: menudescr){
			TextView menuText = new TextView(getActivity());
//			menuText.setMinimumWidth(minWidth);
			LayoutParams lparam =  new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT); //LinearLayout.LayoutParams !!!
			lparam.setMargins(2, 2, 2, 2);
			menuText.setLayoutParams(lparam);
			menuText.setGravity(Gravity.CENTER);
//			menuText.setMinimumWidth(75);
//			menuText.setMinimumHeight(85);
//			menuText.setPadding(1, 1, 1, 1);
//			menuText.setMa
			menuText.setMinEms(5); // WTF ???
			menuText.setBackgroundResource(R.drawable.style_menubtn);
			menuText.setClickable(true);
			menuText.setCompoundDrawablesWithIntrinsicBounds (0, btn.icon, 0, 0);
			menuText.setTextSize(10);
			menuText.setText(btn.name);
//			menuText.setText(" ");
			final int tag= btn.tag;
//			final String command= btn.command;
			menuText.setOnClickListener( new OnClickListener() {	
				@Override
				public void onClick(View v) {
					(	(MainActivity)getActivity()).handler.sendEmptyMessage(tag);
//					mainactivityListener.menubtnPress(command, tag);
//					Log.d(LOG_TAG,"findFragmentById =" + getActivity().getSupportFragmentManager().findFragmentById(R.id.bodyfrgm_fl).getTag());
				}
			});
			menuBtnLL.addView(menuText);
			}
		  }
	  }

	  public void reloadMenu(String tag){
//		  Log.d(LOG_TAG,"reloadMenu =" + getActivity().getSupportFragmentManager().findFragmentById(R.id.bodyfrgm_fl).getTag());
		  constructMenu(tag);
	  }
}
