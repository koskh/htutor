package org.htutor.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import org.htutor.R;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
//import android.R;

public class DbHelper extends SQLiteOpenHelper{
//	private final String createTable = 
	Context context;

	public DbHelper(Context context){
		super(context, "htutor.db", null, 1);
		this.context=context;
	}
	public DbHelper(Context context, String name, CursorFactory factory, int version) {
		super(context, name, factory, version);
		this.context=context;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		try{
			execSqlFile ("startdb.sql", db); //read sql from res/raw/startdb.sql
		} catch (IOException exception) {
			throw new RuntimeException("Database creation error", exception);
		}
		System.out.println("Create table OK");
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		try{
			execSqlFile ("startdb.sql", db);
		} catch (IOException exception) {
			throw new RuntimeException("Database creation error", exception);
		}
		System.out.println("Recreate table OK");
		
	}
//	@Override
//		public void onOpen(SQLiteDatabase db) {
//			super.onOpen(db);
//			db.execSQL("PRAGMA foreign_keys=ON;");
//		}

	
///////////////// Utils
	private void execSqlFile(String sqlFile, SQLiteDatabase db) throws SQLException, IOException {
//		Log.i("execSqlFile:",sqlFile);
		InputStream stream = context.getResources().openRawResource( R.raw.startdb) ;
		Reader streamReader = new InputStreamReader(stream, "UTF-8");
		BufferedReader bufReader = new BufferedReader(streamReader, 8192);
		String sqlRow ="";
		while (( sqlRow=bufReader.readLine() ) != null ) {
			if(	!sqlRow.trim().equals("") && !sqlRow.trim().substring(0, 2).equals("--"))    db.execSQL(sqlRow); // use standard SQL comment
		}
		stream.close();				
	}

}