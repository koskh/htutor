package org.htutor.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.htutor.model.Model;
import org.htutor.view.MainActivity;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
//import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
//import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.AsyncTaskLoader;
//import android.support.v4.content.Loader;
import android.util.Log;

public class TopicsUpdate extends AsyncTaskLoader<String> {
	final String LOG_TAG= "ChecTopicListkUpdate"; //TODO Поглядеть, как взять имя класса из reflection, что бы не писать каждый раз
	private Context context;
	private String updUrl="http://htutor.ru/topics/listfiles.php";
	private File updFolder;
//	private String updateLocalFolder;
	private String command;
	public ArrayList<String> updateTopicList; //= new ArrayList<String>();
	public String  message ;
	
//	public ArrayList<String> TopicName;
	private Model model;
	private Handler handler;
	
	public TopicsUpdate(Context context, Handler handler, Bundle bndl) {
		super(context);
		this.context=context;
		command = bndl.getString("command");
//		updateLocalFolder = bndl.getString("updateLocalFolder");
		updFolder = new File(bndl.getString("updateLocalFolder"));
//		this.bndl=bndl;
//		availableTopic=new ArrayList<String[]>(); 
//		updateInfo="Обновлений нет";
		updateTopicList= new ArrayList<String>();
		this.model = ((MainActivity)context).model;
		this.handler=handler;
		}	

	@Override
	public String loadInBackground() {
//		 error="no error";

				
//		try{
		Log.d(LOG_TAG,"command= " + command);
		
		switch (this.getId()){
			case 0:
				showMessage("Проверяем обновления");
				getLocalTopicList();
				getNetTopicList();
				break;
			case 1:
				showMessage("Начинаем обновление");
				downloadTopic(command);
		
				break;
			case 2:
				showMessage("Начинаем обновление всех топиков");
				getLocalTopicList();
				getNetTopicList();
				for(String topicFile: updateTopicList)
					downloadTopic(topicFile);
				break;
		}
//			if (this.getId()==0) {
//				getTopicList();
//			} else if (this.getId()==1){
//				getTopicList();
//				for(String topicFile: updateTopicList)
//					downloadTopic(topicFile);
//				message= "Все топики обновлены";
//			} else if (this.getId()==2 ){
//				downloadTopic(command);
//			}
//			else if (this.getId()==4 ){ //get local updateTopicsList
//				
//			}
//			else if (this.getId()==5 ){ //unpuck local updateTopics
//				
//			}
		return message;
	}

private void getNetTopicList () { //get Web list topic for update
	try{
		if (!isOnline()) {			
			showMessage( "Для получения списка топиков с сервера нужен интернет" );
			return ;
		}
		// Read aviable remote  topic 
		URL u = new URL(updUrl);
		BufferedReader reader = new BufferedReader(new InputStreamReader(u.openStream()));
		String line;
		while ( (line=reader.readLine()) !=null){ 
			if (model.isNeedUpdate(line))
				updateTopicList.add(line);
		}
		reader.close();
	if (updateTopicList.size()==0) 
		showMessage( "Новых топиков нет" );
	} catch (Exception e) {
		showMessage( "Ошибка сервера обновлений. Попробуйте позже.");
//		Log.d(LOG_TAG, e.toString()); 
		}
	
}
private void getLocalTopicList () { // get aviable local  topics
		if ( ! (updFolder).isDirectory() ) 
			return;
		for ( File f : updFolder.listFiles())
			if ( f.isFile() && f.getName().substring(f.getName().lastIndexOf('.')+1).equals("zip") )
				if(model.isNeedUpdate(f.getName())) updateTopicList.add(f.getName());
	
	
}

private void downloadTopic(String name){
	
	
//	Log.d(	LOG_TAG, "downloadTopic =" + name);
	try{
		InputStream	in=null;
		
		if (	(new File (updFolder.getAbsolutePath()+"/"+name).isFile() ) ){
			in= new FileInputStream(new File (updFolder.getAbsolutePath()+"/"+name) );
		}
		else {
			if (!isOnline()) {			
				showMessage( "Для скачивания топика с сервера нужен интернет" );
				return ;
			}
			in= (new URL("http://htutor.ru/topics/"+name)).openStream();
		}
			OutputStream  out= new FileOutputStream(context.getDir("temp", 0)+"/"+name);	
			byte[] buffer = new byte[8192];
			int read;
			while ((read = in.read(buffer)) != -1) {
				out.write(buffer, 0, read);
			}	     
			out.close();
			in.close();
		
     unpackTopic(name);
     message =name; //return 
	}
		catch (Exception e) { 
			showMessage("Ошибка обновления. Попробуйте позже" );
			Log.d(	LOG_TAG,e.toString()); } 
//	return message;
}

//UTILS
private void  unpackTopic(String topicZipFile){ //topic_name.20140202.zip
	try {
//		Model model =   ((MainActivity)context).model;
		String topicName=  topicZipFile.split("\\.") [0] ;
//		Log.d(LOG_TAG, "topicName" + topicName);
		ZipInputStream zipIn = new ZipInputStream(new FileInputStream(context.getDir("temp", 0)+"/"+topicZipFile)) ;
		ZipEntry zipEntry = null;
//		String filename
		while (	(zipEntry=zipIn.getNextEntry() )!=null){ 		
//			Log.d(LOG_TAG, "Unzipped file " + zipEntry.getName());
			if  (zipEntry.isDirectory() ) 
				return; //we don want work with dirs, its error
			if ( zipEntry.getName().equals(	topicName+ ".txt")  ) {
			//Update Topic|Words into DB
				String  topicVersion =  topicZipFile.split("\\.") [1] ; // Test only
				BufferedReader br = new BufferedReader(new InputStreamReader(zipIn, "UTF-8"));
				String topicDescr=br.readLine(); //assume file have topic descr line, not error check, test only
				model.updateTopic(topicName, topicDescr, topicVersion);
				String line;
				while ( 	( line=br.readLine() )!=null )
					model.updateWord(topicName, line);
			}
			else {
				//copy audio in  folders
				byte[] buffer = new byte[8192];
				int size;
				FileOutputStream outStream = new FileOutputStream (context.getDir("sound", 0)+"/"+ zipEntry.getName().substring(0, 1)+"/"+zipEntry.getName());
				 while((size = zipIn.read(buffer, 0, buffer.length)) != -1) {				 	
					 outStream.write(buffer, 0, size);
//	                    offset=offset+size;
	                }		
				outStream.close();
				zipIn.closeEntry();				
			}
//				Log.d(LOG_TAG, "Unzipped file " +		zipEntry.getName());
		}
		zipIn.close();
		showMessage(topicZipFile + "\n успешно обновлен");	
		
	} catch (Exception e) {  Log.d(	LOG_TAG,e.toString()); }	
	File zipfile= new File(context.getDir("temp", 0)+"/"+topicZipFile);
		zipfile.delete();
		 
	
}
// UTIL
private void showMessage(String message){
//	((MainActivity)getActivity() ).handler.sendEmptyMessage(SHOW_SETTINGS);
	int SHOW_MESSAGE=0;
	Message  msg = handler.obtainMessage( SHOW_MESSAGE,0,0, message);
	handler.sendMessage(msg);
}
private boolean isOnline() {
    ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo netInfo = cm.getActiveNetworkInfo();
    if (netInfo != null && netInfo.isConnected()) {
        return true;
    } else {
        return false;
    }
}

}