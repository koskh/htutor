package org.htutor.util;

import java.io.File;
import java.io.FileDescriptor;

import android.app.Activity;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.media.SoundPool.OnLoadCompleteListener;
import android.net.Uri;
import android.os.Environment;
import android.os.ParcelFileDescriptor;

/*
 * Setup storage
 */
public class Sound implements OnLoadCompleteListener {
	final private String LOG_TAG="Storrage";
	private Context context;
	private SoundPool soundPool;
	private File soundDir;
//	private int streamId;
	
	public Sound (Context context){
		this.context=context;
		soundDir = context.getDir("sound",0);
//		SetupStorage();
//		checkFiles();
		(	(Activity)context).setVolumeControlStream(AudioManager.STREAM_MUSIC);
		soundPool= new SoundPool(2, AudioManager.STREAM_MUSIC, 0);
		soundPool.setOnLoadCompleteListener(this);
	}

	public void SetupStorage(){
		//Create letter folders for files
		StringBuilder letters=new StringBuilder("qwertyuiopasdfghjklzxcvbnm");
		for (int i=0; i<letters.length();i++){
			String name= letters.substring(i, i+1);
			File folder = new File (soundDir+"/"+name);
//			Log.d (LOG_TAG,"folder =" + folder.toString());
			folder.mkdir();
		}		
	}
	
//	public File getSoundFile(String filename){
//		
////		return new File( context.getResources().openRawResource( R.raws.null_sound));
//		return null;
//	}
	public  void playWord( String word){
		//TEST only
//		int file=context.getResources().getIdentifier(word.trim(), "raw", context.getPackageName());
//		if (file !=0)
//			soundPool.load(context, file, 1);
		String folder= word.substring(0, 1);
//		FileDescriptor fd= new FileDescriptor();
//		File soundfile= new File (soundDir+"/"+folder+"/"+word+".ogg");
//		try {
//			ParcelFileDescriptor pfd = ParcelFileDescriptor.open(soundfile,ParcelFileDescriptor.MODE_READ_ONLY);
//			fd= pfd.getFileDescriptor();
//			soundPool.load(fd, 0, 10, 1);
		
//		} catch (Exception e){ e.printStackTrace();}
//		if (soundfile.isFile())
//			soundPool.load("/apple.ogg",1);
			soundPool.load(context.getDir("sound", 0)+"/"+folder+"/"+word+".ogg",1);
			soundPool.load(context.getDir("sound", 0)+"/"+folder+"/"+word+".mp3",1);
//			soundPool.load("/data/data/org.htutor/files/apple.ogg",1);
//			Log (LOG_TAG,"streamId = "+ id);
		
//		MediaPlayer mp = MediaPlayer.create(context, Uri.parse("/data/data/org.htutor/app_sound/a/apple.ogg") );
//		mp.start();
		
	}
//Util
	private void checkFiles(){
		
	}
//Listeners
	@Override
	public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
		// TODO Auto-generated method stub
		soundPool.play(sampleId, 1, 1, 0, 0, 1);
		soundPool.unload(sampleId);
	}
}
