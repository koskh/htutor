-- Включили поддержку FOREIGN KEY
PRAGMA foreign_keys = ON;

DROP TABLE IF EXISTS "Topic";
CREATE TABLE Topic ( id integer primary key, name text, describe text, version integer, active boolean);

DROP TABLE IF EXISTS "Word";
CREATE TABLE Word ( id integer primary key, name text, transcript text, translation text, describe text, audio text, topic integer references topic, score integer, show boolean);
